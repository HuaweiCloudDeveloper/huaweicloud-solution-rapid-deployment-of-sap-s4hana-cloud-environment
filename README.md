[TOC]

**解决方案介绍**
===============
该解决方案可以帮助用户在华为云上一键创建部署SAP S/4HANA 1809高可用所需的云资源。在华为云上部署SAP S/4HANA业务软件，能够充分利用华为云大规格、高性能、高安全和高可靠的能力，以及全生命周期的管理服务，帮助企业简化管理、节省成本、高效运营，快速实现数字化转型。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-sap-s4hana-cloud-environment.html](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-sap-s4hana-cloud-environment.html)

**架构图**
---------------
![方案架构](./document/rapid-deployment-of-sap-s4hana-cloud-environment.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建1个虚拟私有云 VPC和3个子网，用于提供隔离的网络平面。

2、创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。

3、创建3个虚拟IP，用于提供配置高可用所需的虚拟IP。

4、创建4台弹性云服务器 ECS，用于安装SAP S/4HANA 1809软件。

5、创建5个SFS Turbo文件系统，用于提供共享文件存储。

6、创建18个云硬盘 EVS，用于提供云服务器存储。


**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-sap-s4hana-cloud-environment
├── rapid-deployment-of-sap-s4hana-cloud-environment-standard.tf.json -- 标准资源编排模板
├── rapid-deployment-of-sap-s4hana-cloud-environment-ha.tf.json -- 高可用资源编排模板
├── userdate
    ├── init-sap-s4hana-standard.sh   -- 标准资源脚本配置文件
	├── init-sap-s4hana-ha.sh   -- 高可用资源脚本配置文件
```
**开始使用**
---------------
1、打开[华为云服务器控制台](https://console.huaweicloud.com/ecm/?region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)，查看创建的云服务器资源。

![创建的云服务器](./document/readme-image-001.png)

2、跳转到[SAP S4HANA1809同可用区高可用部署最佳实践](https://support.huaweicloud.com/bestpractice-sap/zh-cn_topic_0000001165507753.html)，执行“软件安装”部分。
> [**说明**]
>
> 最佳实践“资源创建”部分除“配置SSH跳转权限”外均已自动化完成，请执行后续步骤即可，如有需要请手动执行“配置SSH跳转权限”。

